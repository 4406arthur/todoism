FROM python:3.7.9 as builder
WORKDIR /src
COPY . .
RUN \ 
  pip3 install --prefix=/install -r requirements.txt


FROM python:3.7.9-alpine3.12
COPY --from=builder /install /usr/local
LABEL MAINTAINER="arthurma <4406arthur@gmail.com>"

#Principle of Least Privilege
#RUN useradd -m app
#ENV PATH="/home/app/.local/bin:${PATH}"
#USER app 

WORKDIR /home/app

COPY . .
RUN \
  flask initdb && \
  flask translate compile

EXPOSE 5000
ENTRYPOINT ["flask"]
CMD ["run", "--host", "0.0.0.0", "--port" ,"5000"]
